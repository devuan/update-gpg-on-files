#!/bin/bash

# this script will get the devuan-keyring package from unstable,
# extract developer and archive keyrings from it and update the
# files archive

AKR=/home/files.devuan.org/public/devuan-archive-keyring.gpg
DKR=/home/files.devuan.org/public/devuan-devs.gpg
OWN=vesta:mirrors
PER=0644
URL=http://pkgmaster.devuan.org/devuan/
DIS=ceres
LOG=/root/update-gpg-on-files.log

function cleanup {
	rm -f Packages.xz keyring.deb data.tar.xz devuan-archive-keyring.gpg devuan-keyring.gpg
	exit $1
}

function error {
	echo "$1"
	cleanup 1
}

{
	flock -n 9 \
		|| error 'Can not get lock'
	wget --timeout=60 "${URL}dists/${DIS}/main/binary-all/Packages.xz" -qO Packages.xz \
		|| error 'Can not download Packages...'
	xzgrep -q '^Filename: pool/main/d/devuan-keyring/devuan-keyring_' Packages.xz \
		|| error 'Can not download Packages...'
	pkg=$(xzgrep '^Filename: pool/main/d/devuan-keyring/devuan-keyring_' Packages.xz | awk '{print $2}')
	wget --timeout=60 "${URL}${pkg}" -qO keyring.deb \
		|| error 'Can not download devuan-keyring...'
	ar x keyring.deb data.tar.xz \
		|| error 'Can not extract data.tar.xz'
	tar --xform 's,^[.]/usr/share/keyrings,.,' -xf data.tar.xz ./usr/share/keyrings/devuan-archive-keyring.gpg ./usr/share/keyrings/devuan-keyring.gpg \
		|| error 'Can not extract keyrings'
	if [ ! -f "${AKR}" ] || ! diff "${AKR}" devuan-archive-keyring.gpg; then
		cp -v devuan-archive-keyring.gpg "${AKR}" &&
		chown -v ${OWN} "${AKR}" &&
		chmod -v ${PER} "${AKR}" \
			|| error "Can not update ${AKR}"
	fi
	if [ ! -f "${DKR}" ] || ! diff "${DKR}" devuan-keyring.gpg; then
		cp -v devuan-keyring.gpg "${DKR}" &&
		chown -v ${OWN} "${DKR}" &&
		chmod -v ${PER} "${DKR}" \
			|| error "Can not update ${DKR}"
	fi
} 9<$0 2>&1 | awk '{print strftime("[%Y-%m-%d %H:%M:%S]"),$0}' >> ${LOG}

cleanup 0
