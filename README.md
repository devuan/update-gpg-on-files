# update-gpg-on-files

This simple script updates GPG keyrings on [files.devuan.org](https://files.devuan.org) by
- fetching `devuan-keyring` package from `ceres`
- extracting archive and developer keyrings
- publishing them with the appropriate ownership and permissions
- there is a locking mechanism against concurrent execution
